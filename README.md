# About me

I worked as Software Engineer since 2010. After spending 4 years in Switzerland I was thinking about my future. And very soon I found out that remote job is something that could work for me. I've been working remotely since 2016 and I have no plans to change that - ever. I've been with GitLab since 2017 and I still enjoy working here.

I live in a village very close to Pilsen, Czech Republic (you might know Pilsner beer 🍺) and have 2 small kids (4 and almost 1.5, as of March 2022).

# How do I work

I am most productive in mornings when I feel I can really focus. I am usually available until 18-20 CET (if needed) but meetings after 15 CET should be scheduled ahead of time as I need to communicate those with my husband/someone else so that they can take care of kids. During the day I am usually not at computer for the whole time, some days I work evenings instead of mornings.

## Communication

I prefer async communication as it gives me much more flexibility, which is very important for me, espacially with two small kids.

I try to respond on **Slack** quicky, at least with a short message about my availability. 

I also try to take care of mentions on **GitLab** but sometimes it might take some time before I am able to answer - especially if the topic is complex.

I prefer to have a prepared agenda before joining a **Zoom** meeting.

## Notes

- I am good in splitting tasks into smaller pieces and I like iterating over issues - I want to deliver at least something pretty soon
- I was developing GitLab for more than 5 years so I can understand the technical challenges and respective code pretty quickly
- English is not my mother tongue, sometimes I may misunderstand or say something incorrectly which might be misunderstood
- I am a manager for the first time, responsibilities and tasks that are obvious for others might take a bit longer to me.
- I am a morning person, less productive during evenings (which has been changing since having the second child)
